// // створити функцію під назвоню max яка приймає два параметри у вигляді чисел, функція повинна перевірити яке число більше і його повернути у вигляді результату, якщо числа рівні то вернути або перше або друге все рівно
//
// function max(a, b) {
//     if (a === b) {
//         return a
//     } else
//         return Math.max(a, b)
// }
//
// console.log(max(5, 9));
//
// // створити функцію яка буде формувати масив з випадкових чисел, кількість передаємо через параметри, межі випадкових чисел також передаємо, тобто всього їх три, результат сформований масив
//
// function createArray(a, b, c) {
//     let array = [];
//
//     for (let i = 0; i < a; i++) {
//         array.push(i);
//         array[i] = Math.floor(Math.random() * (b - c) + c)
//     }
//     return array
// }
//
// console.log(createArray(10, 40, 60));

// (задача з зірочкою) написасти функцію яка приймає в параметри два масива і функцію колбек callback яка зможе реалізовувати перевірку кожного елементу які будуть обєднуватись в результуючий масив і повертатись в результаті

function createArray(callback, arr1, arr2) {
    return callback(arr1).concat(callback(arr2));
}

console.log(createArray((arr) => {
    let pairedNumbersArr = [];
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] % 2 === 0) {
            pairedNumbersArr.push(arr[i])
        }
    }
    return pairedNumbersArr
}, [1, 6, 3, 7, 5], [3, 8, 5, 6, 3, 7]));
